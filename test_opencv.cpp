#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/cudaimgproc.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

using namespace cv;
using namespace std;

#define ASSERT(exp, msg) if(!(exp)) { cerr << msg << endl; exit(-1); }

// void on_trackbar_ndisparities(int value, void*) {
//   sbm->setNumDisparities((value+1)*16);
// }

void createGUI() {
  // const char* windowGUI = "gui";
  // namedWindow(windowGUI, WINDOW_AUTOSIZE);
  // createTrackbar("BlockSize", windowGUI, &BlockSize, 125, on_trackbar_blockSize);
}

int main(int argc, char ** argv) {

  ASSERT(argc >= 3, "usage: " << argv[0] << " <INPUT_VID_DEV#> <V4L2_LB_DEV#>");

  int input_dev_id = atoi(argv[1]);
  int v4l2_loopback = atoi(argv[2]);

  // NOTE: gotta explicitly set the v4l2 backend, otherwise gstreamer is used
  VideoCapture cap(input_dev_id, CAP_V4L2);
  const int MAX_SIZE = 100000;
  ASSERT(cap.set(CAP_PROP_FRAME_WIDTH, MAX_SIZE), "failed to set width");
  ASSERT(cap.set(CAP_PROP_FRAME_HEIGHT, MAX_SIZE), "failed to set height");
  ASSERT(cap.set(CAP_PROP_FOURCC, VideoWriter::fourcc('M', 'J', 'P', 'G')), "FourCC not supported!");

  // ASSERT(cap.set(CAP_PROP_FRAME_WIDTH, 640), "failed to set width");
  // ASSERT(cap.set(CAP_PROP_FRAME_HEIGHT, 580), "failed to set height");
  // ASSERT(cap.set(CAP_PROP_FOURCC, VideoWriter::fourcc('Y', 'U', 'Y', 'V')), "FourCC not supported!");

  ASSERT(cap.isOpened(), "FAILED to initialize video capture!");

  const char* windowName1 = "Webcam feed 1";
  namedWindow(windowName1, WINDOW_AUTOSIZE);

  // createGUI();

  int width = 640;
  int height = 480;
  int vidsendsiz = 0;

  stringstream ss;
  ss << "/dev/video" << v4l2_loopback;

  int v4l2lo = open(ss.str().c_str(), O_WRONLY);
  ASSERT(v4l2lo >= 0, "Error opening v4l2lb device: " << strerror(errno));

  {
    v4l2_format v;
    v.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

    int t = ioctl(v4l2lo, VIDIOC_G_FMT, &v);
    if( t < 0 ) {
      exit(t);
    }
    v.fmt.pix.width = width;
    v.fmt.pix.height = height;
    v.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
    vidsendsiz = width*height*3;
    t = ioctl(v4l2lo, VIDIOC_S_FMT, &v);
    if(t < 0) { exit(t); }
  }

  while(true) {
    Mat frame1;

    ASSERT(cap.grab(), "Error reading frame from camera feed");

    ASSERT(cap.retrieve(frame1), "error retriving frame from cap");

    imshow(windowName1, frame1);

    auto grayFrame = Mat(frame1.rows, frame1.cols, CV_8UC1);
    cvtColor(frame1, grayFrame, COLOR_RGB2GRAY);

    cuda::GpuMat dst, src;
    src.upload(grayFrame);

    Ptr<cuda::CLAHE> ptr_clahe = cuda::createCLAHE(5.0, Size(8, 8));
    ptr_clahe->apply(src, dst);

    dst.download(grayFrame);

    auto f1 = Mat(grayFrame.rows, grayFrame.cols, CV_8UC3);
    // frame1.copyTo(f1);
    cvtColor(grayFrame, f1, COLOR_GRAY2RGB);
    if(f1.rows != height || f1.cols != width) {
      auto dst = Mat(height, width, CV_8UC3);
      resize(f1, dst, dst.size(), 0, 0, INTER_LINEAR);
      f1 = dst;
    }

    int size = f1.total() * f1.elemSize();

    ASSERT(size == vidsendsiz, "size != vidsendsiz " << size << " / " << vidsendsiz);

    size_t written = write(v4l2lo, f1.data, size);
    ASSERT(written >= 0, "Error writing to v4l2lo device");

    switch(waitKey(10)) {
      case 27:
        return 0;
    }
  }

  return 0;
}