# build file for adobeChallenge
cmake_minimum_required (VERSION 3.10)
project (uvccapture)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(OpenCV
  PATHS /home/benjamin/Downloads/opencv-4.5.0/build 
)

include_directories( ${OpenCV_INCLUDE_DIRS} )

add_executable(${CMAKE_PROJECT_NAME} test_opencv.cpp)

target_link_libraries(${CMAKE_PROJECT_NAME} ${OpenCV_LIBS})

# set(SOURCES 
#   ${SOURCES}
#   ${CMAKE_CURRENT_SOURCE_DIR}/capture.cpp

#   # Just link the format file of fmt to simplify building
#   # ${CMAKE_CURRENT_SOURCE_DIR}/fmt/src/format.cc
# )

# add_executable (${CMAKE_PROJECT_NAME} ${SOURCES})

# target_compile_definitions(${CMAKE_PROJECT_NAME} PUBLIC LINUX)

# Link to the include files
# target_include_directories(${CMAKE_PROJECT_NAME} 
#   PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/fmt/include
# )
